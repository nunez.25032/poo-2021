package com.aplicacion.backend.aplicacionws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplicacionwsApplication {

    public static void main(String[] args) {
        SpringApplication.run(AplicacionwsApplication.class, args);
    }

}
