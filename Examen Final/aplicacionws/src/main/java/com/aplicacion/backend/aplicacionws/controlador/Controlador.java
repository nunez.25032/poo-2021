package com.aplicacion.backend.aplicacionws.controlador;


import com.aplicacion.backend.aplicacionws.dto.RespuestaUsuario;
import com.aplicacion.backend.aplicacionws.servicio.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"*"})
public class Controlador {
    @Autowired
    private Servicio servicio;

    @RequestMapping(
            value = "/autenticar-usuario",
            method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody RespuestaUsuario autenticarUsuario(){
        RespuestaUsuario respuestaUsuario = new RespuestaUsuario();
        return respuestaUsuario;
    }
}
