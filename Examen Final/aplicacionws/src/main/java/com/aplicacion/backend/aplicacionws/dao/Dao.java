package com.aplicacion.backend.aplicacionws.dao;

import com.aplicacion.backend.aplicacionws.dto.Usuario;

public interface Dao {
    public Usuario autenticarUsuario(Usuario usuario);
    public Usuario busquedaUsuarios(Usuario usuario);
    public Usuario actualizacionDatos(Usuario usuarios);

    Usuario autenticarUsuario();
}
