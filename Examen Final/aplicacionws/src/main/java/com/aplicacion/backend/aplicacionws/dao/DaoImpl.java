package com.aplicacion.backend.aplicacionws.dao;

import com.aplicacion.backend.aplicacionws.dto.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;

@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void cerrarConexion(ResultSet resultado, Statement sentencia){
        try {
            if(resultado != null){
                resultado.close();
            }
            if(sentencia != null){
                sentencia.close();
            }
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }



    private Usuario traeUsuario(ResultSet resultado) throws SQLException {
        Usuario usuario = new Usuario(
                resultado.getInt("id_usuario"),
                resultado.getString("nombres"),
                resultado.getString("apellidos"),
                resultado.getString("correo"),
                resultado.getString("admistrador"),
                resultado.getString("clave")
        );
        return usuario;
    }

    public Usuario autenticarUsuario(Usuario usuario){
        String sql = " select id_usuario,nombres,apellidos,correo,administrador,clave from usuario\n" +
                " where administrador = ?";

        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1,usuario.getId_usuario());
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                usuario = traeUsuario(resultado);
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }


    public Usuario busquedaUsuarios(Usuario usuario) {
        return null;
    }


    public Usuario actualizacionDatos(Usuario usuarios) {
        return null;
    }

    @Override
    public Usuario autenticarUsuario() {
        return null;
    }

}
