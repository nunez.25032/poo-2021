package com.aplicacion.backend.aplicacionws.dto;

import lombok.Data;

import java.util.List;

@Data
public class RespuestaUsuario {
    private List<Usuario> lista;
}
