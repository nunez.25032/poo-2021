package com.aplicacion.backend.aplicacionws.dto;

import lombok.Data;

@Data
public class Usuario {
    private Integer id_usuario;
    private String nombres;
    private String apellidos;
    private String correo;
    private String administrador;
    private String clave;

    public Usuario(Integer id_usuario, String nombres, String apellidos, String correo, String administrador, String clave) {
        this.id_usuario = id_usuario;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.administrador = administrador;
        this.clave = clave;
    }
}
