package com.aplicacion.backend.aplicacionws.servicio;


import com.aplicacion.backend.aplicacionws.dto.Usuario;


public interface Servicio {
    public Usuario autenticarUsuario();
    public Usuario busquedaUsuarios(Usuario usuario);
    public Usuario actualizacionDatos(Usuario usuarios);


}