package com.aplicacion.backend.aplicacionws.servicio;

import com.aplicacion.backend.aplicacionws.dao.Dao;
import com.aplicacion.backend.aplicacionws.dto.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao dao;

    public Usuario autenticarUsuario() {
        return dao.autenticarUsuario();
    }

    public Usuario busquedaUsuarios(Usuario usuario) {
        return null;
    }


    public Usuario actualizacionDatos(Usuario usuarios) {
        return null;
    }

}
