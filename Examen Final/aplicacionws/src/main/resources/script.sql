CREATE TABLE usuario(
                        id_usuario NUMERIC(9),
                        nombres VARCHAR(100),
                        apellidos VARCHAR(100),
                        correo VARCHAR(200),
                        administrador CHAR(1),
                        clave VARCHAR(200)
);

ALTER TABLE usuario ADD PRIMARY KEY (id_usuario);

INSERT INTO usuario(id_usuario,nombres,apellidos,correo,administrador,clave)
VALUES(1,'Gean Piero','Nunez Poma','robert@gmail.com','1','123');

INSERT INTO usuario(id_usuario,nombres,apellidos,correo,administrador,clave)
VALUES(2,'Robert Jesus','Martinez Loza','jesus@gmail.com','','456');

INSERT INTO usuario(id_usuario,nombres,apellidos,correo,administrador,clave)
VALUES(3,'Emilio Jose','Rojas Pereira','jose@gmail.com','','789');

select id_usuario,nombres,apellidos,correo,administrador,clave from usuario
where administrador = '1';

SELECT id_usuario,nombres,apellidos,correo,administrador,clave FROM usuario
WHERE administrador = '';