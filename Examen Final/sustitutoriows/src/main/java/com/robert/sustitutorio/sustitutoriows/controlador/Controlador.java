package com.robert.sustitutorio.sustitutoriows.controlador;


import com.robert.sustitutorio.sustitutoriows.dto.modell.Curso;
import com.robert.sustitutorio.sustitutoriows.dto.modell.Estudiante;

import com.robert.sustitutorio.sustitutoriows.servicio.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"*"})
public class Controlador {
    @Autowired
    private Servicio servicio;

    @RequestMapping(
            value = "autenticar-estudiante",method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody
    Estudiante autenticarEstudainte(@RequestBody Estudiante estudiante, Curso curso){
            return servicio.autenticarEstudiante(estudiante,curso);
    }


}
