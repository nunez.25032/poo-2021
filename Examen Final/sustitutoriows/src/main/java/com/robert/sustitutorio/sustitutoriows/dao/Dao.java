package com.robert.sustitutorio.sustitutoriows.dao;


import com.robert.sustitutorio.sustitutoriows.dto.modell.Curso;
import com.robert.sustitutorio.sustitutoriows.dto.modell.Estudiante;

import java.util.List;

public interface Dao {

    public Curso auteticarEstudiante(Estudiante estudiante, Curso curso);

}
