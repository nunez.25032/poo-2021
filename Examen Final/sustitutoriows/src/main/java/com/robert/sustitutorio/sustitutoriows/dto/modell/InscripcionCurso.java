package com.robert.sustitutorio.sustitutoriows.dto.modell;

public class InscripcionCurso {
    private Curso idCurso;
    private Estudiante idEstudiante;
    private String fechaInscripcion;
    /*
    	id_curso NUMERIC(9),
	id_estudiante NUMERIC(9),
	fecha_inscripcion VARCHAR(10)
     */

    public InscripcionCurso(Curso idCurso, Estudiante idEstudiante, String fechaInscripcion) {
        this.idCurso = idCurso;
        this.idEstudiante = idEstudiante;
        this.fechaInscripcion = fechaInscripcion;
    }
}
