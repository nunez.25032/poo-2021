CREATE TABLE estudiante(
                           id_estudiante NUMERIC(9) PRIMARY KEY,
                           nombres VARCHAR(200),
                           apellidos VARCHAR(200),
                           correo VARCHAR(200) UNIQUE,
                           clave VARCHAR(200)
);


CREATE TABLE curso(
                      id_curso NUMERIC(9) PRIMARY KEY,
                      nombre VARCHAR(200),
                      precio NUMERIC(9,2),
                      fecha_inicio VARCHAR(10),
                      fecha_fin VARCHAR(10)
);

CREATE TABLE inscripcion_curso(
                                  id_curso NUMERIC(9),
                                  id_estudiante NUMERIC(9),
                                  fecha_inscripcion VARCHAR(10)
);

ALTER TABLE inscripcion_curso ADD FOREIGN KEY(id_curso)
    REFERENCES curso(id_curso);

ALTER TABLE inscripcion_curso ADD FOREIGN KEY(id_estudiante)
    REFERENCES estudiante(id_estudiante);


ALTER TABLE inscripcion_curso ADD PRIMARY KEY (id_curso);
ALTER TABLE inscripcion_curso ADD PRIMARY KEY (id_estudiante);


INSERT INTO estudiante(id_estudiante,nombres,apellidos,correo,clave) VALUES(1,'Robert','Nunez','robert@gmail.com','dbjava');
INSERT INTO estudiante(id_estudiante,nombres,apellidos,correo,clave) VALUES(2,'Gean','Martinez','gean@gmail.com','ocho8');
INSERT INTO estudiante(id_estudiante,nombres,apellidos,correo,clave) VALUES(3,'Piero','Tagliafico','piero@gmail.com','season3');

INSERT INTO curso(id_curso,nombre,precio,fecha_inicio,fecha_fin) VALUES(1,'POO',30.50,'02/08/2021','02/10/2021');
INSERT INTO curso(id_curso,nombre,precio,fecha_inicio,fecha_fin) VALUES(2,'Multivariable',27.80,'15/04/2021','15/06/2021');
INSERT INTO curso(id_curso,nombre,precio,fecha_inicio,fecha_fin) VALUES(3,'Algoritmia',35.06,'24/07/2021','24/09/2021');

SELECT *
FROM inscripcion_curso;

INSERT INTO inscripcion_curso(id_curso,id_estudiante,fecha_inscripcion) VALUES(2,1,'06/03/2021');
INSERT INTO inscripcion_curso(id_curso,id_estudiante,fecha_inscripcion) VALUES(1,3,'18/07/2021');
INSERT INTO inscripcion_curso(id_curso,id_estudiante,fecha_inscripcion) VALUES(3,2,'25/06/2021');



SELECT nombre,precio,fecha_inicio,fecha_fin
FROM curso c
         JOIN inscripcion_curso i ON (c.id_curso = i.id_curso)
         JOIN estudiante e ON (e.id_estudiante = i.id_estudiante)
WHERE correo = 'robert@gmail.com' AND clave = 'dbjava'