package examenParcial.pregunta1;

public class Busqueda {
	private String titulo;
	private String nombreAutores;
	private String codigo;
	
	public Busqueda(String titulo, String nombreAutores, String codigo) {
		super();
		this.titulo = titulo;
		this.nombreAutores = nombreAutores;
		this.codigo = codigo;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getNombreAutores() {
		return nombreAutores;
	}
	public void setNombreAutores(String nombreAutores) {
		this.nombreAutores = nombreAutores;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	

}
