package examenParcial.pregunta1;

import java.util.ArrayList;
import java.util.List;

public class Libro {
	private String descripcion;
	private String editorial;
	private String resumen;
	private String anioPublicacion;
	private ArrayList<Titulo> titulo;
	
	public Libro(String descripcion, String editorial, String resumen, String anioPublicacion,
			ArrayList<Titulo> titulo) {
		super();
		this.descripcion = descripcion;
		this.editorial = editorial;
		this.resumen = resumen;
		this.anioPublicacion = anioPublicacion;
		this.titulo = titulo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getEditorial() {
		return editorial;
	}
	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}
	public String getResumen() {
		return resumen;
	}
	public void setResumen(String resumen) {
		this.resumen = resumen;
	}
	public String getAnioPublicacion() {
		return anioPublicacion;
	}
	public void setAnioPublicacion(String anioPublicacion) {
		this.anioPublicacion = anioPublicacion;
	}
	public ArrayList<Titulo> getTitulo() {
		return titulo;
	}
	public void setTitulo(ArrayList<Titulo> titulo) {
		this.titulo = titulo;
	}
	
	
	
	
	
	
	
	
	
	
}
