package examenParcial.pregunta3;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class Aplicacion {
	public static void main(String[] args) {
		List<VentaRelojes> reloj= new ArrayList<VentaRelojes>();
		VentaRelojes relojito1 = new VentaRelojes("producto A",8,45.7);
		reloj.add(relojito1);
		VentaRelojes relojito2 = new VentaRelojes("producto B",3,23.7);
		reloj.add(relojito2);
		VentaRelojes relojito3 = new VentaRelojes("producto C",10,12.2);
		reloj.add(relojito3);	
		
		llenarMapa(mapa, reloj);
		
		
	}
	
	private static void llenarMapa(Map<Integer, Integer> mapa, List<Integer> lista) {
		for(Integer item : lista) {
			if(mapa.containsKey(item)) {
				mapa.replace(item, mapa.get(item)+1);
			}
			else {
				mapa.put(item,1 );
			}
		}
	}
	
}
