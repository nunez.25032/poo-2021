package examenParcial.pregunta3;

public class VentaRelojes extends TotalVendido {
	private String producto;
	private int cantidad;
	private double precioReloj;
	private double total;
	public VentaRelojes(String producto, int cantidad, double precioReloj) {
		super();
		this.producto = producto;
		this.cantidad = cantidad;
		this.precioReloj = precioReloj;
	}
	
	public double calcularTotal() {
		double totalVendido=0;
		totalVendido = cantidad*precioReloj;
		return totalVendido;
	}
	
	
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public double getPrecioReloj() {
		return precioReloj;
	}
	public void setPrecioReloj(double precioReloj) {
		this.precioReloj = precioReloj;
	}
	
	
	
	
	
}
