package pe.edu.uni.fiis.primerapractica.pregunta2;

import java.util.Scanner;

public class Aplicacion {
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.println("Ingrese el lado: ");
		double ladoPrincipal;
		ladoPrincipal=entrada.nextDouble();
		
		Diagonales usar = new Diagonales();
		
		
		Triangulo objtri=new Triangulo();		
		objtri.setLado(ladoPrincipal);
		
		System.out.println("Triangulo");
		System.out.println("Area: "+objtri.calcularAreaTriangulo());
		System.out.println("Periemtro: "+objtri.calcularPerimetroTriangulo());
		System.out.println("Diagonales: "+ usar.numeroDiagonales(objtri.calcularPerimetroTriangulo()/ladoPrincipal));
		
		Cuadrado cua = new Cuadrado();
		cua.setLado(ladoPrincipal);
		
		System.out.println("Cuadrado");
		System.out.println("Area: "+cua.calcularAreaCuadrado());
		System.out.println("Periemtro: "+cua.calcularPerimetroCuadrado());
		System.out.println("Diagonales: "+ usar.numeroDiagonales(cua.calcularPerimetroCuadrado()/ladoPrincipal));
		
		
		pentagono pen = new pentagono();
		pen.setLado(ladoPrincipal);
		
		System.out.println("Pentagono");
		System.out.println("Area: "+pen.calcularAreaPentagono());
		System.out.println("Periemtro: "+pen.calcularPerimetroPentagono());
		System.out.println("Diagonales: "+ usar.numeroDiagonales(pen.calcularPerimetroPentagono()/ladoPrincipal));
		
		Hexagono hex = new Hexagono();
		hex.setLado(ladoPrincipal);
		
		System.out.println("Hexagono");
		System.out.println("Area: "+ hex.calcularAreaHexagono());
		System.out.println("Periemtro: "+hex.calcularPerimetroHexagono());
		System.out.println("Diagonales: "+ usar.numeroDiagonales(hex.calcularPerimetroHexagono()/ladoPrincipal));
		
		
		
	}
}
