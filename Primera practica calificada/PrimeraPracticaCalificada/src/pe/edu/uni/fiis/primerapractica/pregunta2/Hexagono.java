package pe.edu.uni.fiis.primerapractica.pregunta2;

public class Hexagono {
	public double lado;


	public double getLado() {
		return lado;
	}

	public void setLado(double lado) {
		this.lado = lado;
	}
	
	public double calcularAreaHexagono() {
		double area;
		double primero=Math.pow(lado, 2);
		double segundo=Math.sqrt(3);
		area=(6*primero*segundo)/4;
		return area;
	}
	
	public double calcularPerimetroHexagono() {
		return lado*6;
	}

}
