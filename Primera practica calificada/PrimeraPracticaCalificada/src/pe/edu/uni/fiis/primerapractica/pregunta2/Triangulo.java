package pe.edu.uni.fiis.primerapractica.pregunta2;

public class Triangulo {
	private double lado;

	public double getLado() {
		return lado;
	}

	public void setLado(double lado) {
		this.lado = lado;
	}
	
	public double calcularAreaTriangulo() {
		double altura; 
		altura=(lado/2)*Math.sqrt(3);
		double area; 
		area = altura*lado;
		return area;
	}
	
	public double calcularPerimetroTriangulo() {
		double perimetro; 
		perimetro= 3*lado;
		return perimetro;
	}
	
	
}
