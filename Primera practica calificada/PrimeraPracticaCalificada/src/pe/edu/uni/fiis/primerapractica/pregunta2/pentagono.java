package pe.edu.uni.fiis.primerapractica.pregunta2;

public class pentagono {
	private double lado;

	public double getLado() {
		return lado;
	}

	public void setLado(double lado) {
		this.lado = lado;
	}
	
	public double calcularPerimetroPentagono() {
		return 5*lado;
	}
	
	public double calcularAreaPentagono() {
		double area;
		double radio;
		radio=lado*(1/Math.sqrt(10-2*Math.sqrt(5)));
		double apotema;
		apotema=radio*(Math.sqrt(5)+1);
		area= (calcularPerimetroPentagono()*apotema)/2;
		return area;
	}
	
	
}
