package pe.uni.edu.uni.fiis.semana5.HojaInteractiva;



public class Aplicacion {
	public static void main(String[] args) {
		System.out.println("TRIANGULO");
		Figura triangulo = new Triangulo(8);
		System.out.println(triangulo.calcularArea());
		System.out.println(triangulo.calcularPerimetro());
		
		System.out.println("CUADRADO");
		Figura cuadrado = new Cuadrado(8);
		System.out.println(cuadrado.calcularArea());
		System.out.println(cuadrado.calcularPerimetro());
		
		System.out.println("PENTAGONO");
		Figura pentagono = new Pentagono(8);
		System.out.println(pentagono.calcularArea());
		System.out.println(pentagono.calcularPerimetro());
		
		System.out.println("HEXAGONO");
		Figura hexagono = new Hexagono(8);
		System.out.println(hexagono.calcularArea());
		System.out.println(hexagono.calcularPerimetro());
		
		System.out.println("HEPTAGONO");
		Figura heptagono = new Heptagono(8);
		System.out.println(heptagono.calcularArea());
		System.out.println(heptagono.calcularPerimetro());
	}
}
