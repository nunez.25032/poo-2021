package pe.uni.edu.uni.fiis.semana5.HojaInteractiva;

public class Cuadrado extends Figura {
	private double lado;
	public Cuadrado(double lado) {
		super(lado);
	}
	
	public double calcularArea() {
		
		return Math.pow(super.getLado(), 2);
	}
	
	public double calcularPerimetro() {
	
		return 4*super.getLado();
	}
	

}
