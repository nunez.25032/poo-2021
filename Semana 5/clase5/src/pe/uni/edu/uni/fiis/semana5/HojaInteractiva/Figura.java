package pe.uni.edu.uni.fiis.semana5.HojaInteractiva;

public abstract class Figura {
	private double lado;
	public abstract double calcularArea();
	public abstract double calcularPerimetro();
	public Figura(double lado) {
		super();
		this.lado = lado;
	}
	public double getLado() {
		return lado;
	}
	public void setLado(double lado) {
		this.lado = lado;
	}

	
}
