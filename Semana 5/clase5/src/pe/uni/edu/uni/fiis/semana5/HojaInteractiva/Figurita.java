package pe.uni.edu.uni.fiis.semana5.HojaInteractiva;

public interface Figurita {
	public abstract double calcularArea();
	public abstract double calcularPerimetro();
}
