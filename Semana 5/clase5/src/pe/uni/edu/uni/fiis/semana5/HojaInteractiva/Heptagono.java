package pe.uni.edu.uni.fiis.semana5.HojaInteractiva;

public class Heptagono extends Figura {
	
	
	public Heptagono(double lado) {
		super(lado);
		
	}

	public static double apotema(double ladito) {
		return ladito/0.9631;
	}
	
	public double calcularArea() {
		
		return (7*super.getLado()*apotema(super.getLado()))/2;
	}

	
	public double calcularPerimetro() {
	
		return 7*super.getLado();
	}
}
