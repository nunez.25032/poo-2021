package pe.uni.edu.uni.fiis.semana5.HojaInteractiva;

public class Hexagono extends Figura {
	public Hexagono(double lado) {
		super(lado);
	}
	
	public double calcularArea() {
		
		return (((Math.pow(super.getLado(), 2))*Math.sqrt(3))*3)/2;
	}

	
	public double calcularPerimetro() {
	
		return 6*super.getLado();
	}
}
