package pe.uni.edu.uni.fiis.semana5.HojaInteractiva;

public class Pentagono extends Figura {
	
	public Pentagono(double lado) {
		super(lado);
	}

	public static double apotema(double ladito) {
		return ladito/1.45;
	}
	
	public double calcularArea() {
		
		return (5*super.getLado()*apotema(super.getLado()))/2;
	}

	
	public double calcularPerimetro() {
	
		return 5*super.getLado();
	}
	
}
