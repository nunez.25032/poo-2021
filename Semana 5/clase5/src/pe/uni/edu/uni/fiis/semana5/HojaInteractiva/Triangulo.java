package pe.uni.edu.uni.fiis.semana5.HojaInteractiva;

public class Triangulo extends Figura {
		
	public Triangulo(double lado) {
		super(lado);
	}

	public double calcularArea() {
		
		return ((Math.pow(super.getLado(), 2)*Math.sqrt(3))/4);
	}

	public double calcularPerimetro() {
	
		return 3*super.getLado();
	}

}
