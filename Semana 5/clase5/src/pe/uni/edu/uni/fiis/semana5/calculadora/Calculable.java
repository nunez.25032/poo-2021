package pe.uni.edu.uni.fiis.semana5.calculadora;

public interface Calculable {
	public abstract double mover(int pasos);//no es necesario poner el public abstract, pues en interface es como una regla interior.
	public abstract double restar(int primero, int segundo, Signo signo);
	
	//las interfaces obligan a las clases implementar los metodos de las interfaces
}
