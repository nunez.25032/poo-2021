package pe.uni.edu.uni.fiis.semana5.calculadora;

import pe.uni.edu.uni.fiis.semana5.singleton.SingletonVehiculo;
import pe.uni.edu.uni.fiis.semana5.singleton.Vehiculo;

public class Calculadora implements Calculable, Contable{
	public static void main(String[] args) {
		//Vehiculo vehiculo = new Vehiculo("232", 14.5, 9);
		//ya no me deja crear el objeto desde aca
		
		//----Vehiculo vehiculo = SingletonVehiculo.getInstancia();
		//Vehiculo vehiculo1 = SingletonVehiculo.getInstancia();
		
		//vehiculo 1 y vehiculoe  sel mismo
		Calculadora calculadora = new Calculadora();
		calculadora.restar(12,15,Signo.RESTA);
	}
	//en cualquier clase que se implemente Calculabre se debe poner el metodo que esta en Calculable
	
	public double mover(int pasos) {
		Vehiculo vehiculo = SingletonVehiculo.getInstancia();
		vehiculo.avanzar(pasos);
		return vehiculo.getPosicion();
		//se puede utilizar el singleton 
	}

	public double restar(int primero, int segundo, Signo signo) {
		System.out.println(signo.getSimbolo());
		return primero-segundo;
	}
	
	public void contar(int limiteInferior, int limiteSuperior) {
		for (int i = limiteInferior; i < limiteSuperior; i++) {
			System.out.println(i);
		}
		
	}
	
	
}
