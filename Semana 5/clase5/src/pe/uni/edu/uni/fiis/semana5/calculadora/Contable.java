package pe.uni.edu.uni.fiis.semana5.calculadora;

public interface Contable {
	
	void contar(int limiteInferior, int limiteSuperior);
}
