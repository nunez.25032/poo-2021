package pe.uni.edu.uni.fiis.semana5.calculadora;

public enum Signo {
	SUMA("suma","+"),RESTA("resta","-"),PRODUCTO("multiplicacion",".");
	private String operacion;
	private String simbolo;
	
	private Signo(String operacion, String simbolo) {
		this.operacion = operacion;
		this.simbolo = simbolo;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getSimbolo() {
		return simbolo;
	}

	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}
	
	
	
	
	
	
	

}
