package pe.uni.edu.uni.fiis.semana5.excepcion;

public class Aplicacion {
	public static void main(String[] args) {
		int a = 0;
		int b = 2;
		try {
			mostrar(a,b);
		} catch (MiException e) {
			e.printStackTrace();//<--esto es para pintar el mensaje de abajo "a no debe ser menor a b"
			
		}
		finally {
			System.out.println("Final");
		}
		
		System.out.println("Final del programa");
		
	}
	//Si deice "at" ha ocurrido un error hecho
	
	//con throws estoy propagando la excepcion
	public static void mostrar(int a, int b) throws MiException{
		if(a<b) {
			throw new MiException("a no debe ser menor b");
		}
		System.out.println(b);
	}
}
