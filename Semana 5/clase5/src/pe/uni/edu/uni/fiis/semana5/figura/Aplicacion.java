package pe.uni.edu.uni.fiis.semana5.figura;

public class Aplicacion {
	public static void main(String[] args) {
		Circulo circulo = new Circulo();
		circulo.setRadio(34);
		Cuadrado cuadrado = new Cuadrado();
		cuadrado.setLado(32);
		System.out.println(sumarAreaYPerimetro(circulo));
		System.out.println(sumarAreaYPerimetro(cuadrado));
		
		
		Circulo circuloNuevo = new CirculoNuevo();
		circuloNuevo.setRadio(34);
		/*
		System.out.println(circulo.calcularArea());
		System.out.println(circuloNuevo.calcularArea());
		System.out.println(circulo.calcularPerimetro());
		System.out.println(circuloNuevo.calcularPerimetro());
		*///NO SE DEBERIA CODIFICAR ASI PARA EL POLIMORFISMO
		
		//con el segundo metodo se deberia usar el polimorfismo
		mostrarMensaje(circulo);
		mostrarMensaje(circuloNuevo);
	}
	
	public static double sumarAreaYPerimetro(Figura figura) {
		return figura.calcularArea() +figura.calcularPerimetro();
	}
	/*
	public static double sumarAreaYPerimetro(Cuadrado cuadrado) {
		return cuadrado.calcularArea() +cuadrado.calcularPerimetro();
	}
	public static double sumarAreaYPerimetro(Circulo circulo) {
		return circulo.calcularArea() +circulo.calcularPerimetro();
	}
	*/
	
	/*public static double sumarAreaYPerimetro(Circulo circulo) {
		return circulo.calcularArea() +circulo.calcularPerimetro();
	}
	//        |
	//POLIMORFISMO(PARA USAR POLIMORFISMO UTILIZO SUPER CALSES O LAS INTERFACES)eL DE ARRIBA YA ESTA CON POLIMORFISMO
	//        | 
	public static double sumarAreaYPerimetro(Cuadrado cuadrado) {
		return cuadrado.calcularArea() +cuadrado.calcularPerimetro();
	}
	*/
	//con este metdo es mejor usar el polimorfismo
	public static void mostrarMensaje(Circulo circulo) {
		System.out.println(circulo.calcularArea());
		System.out.println(circulo.calcularPerimetro());
		
		
	}
	

}
