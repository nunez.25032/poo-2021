package pe.uni.edu.uni.fiis.semana5.figura;

public class Circulo /*implements Figurable*/extends Figura {
	private double radio;
	
	
	
	public double getRadio() {
		return radio;
	}


	public void setRadio(double radio) {
		this.radio = radio;
	}


	public double calcularArea() {
		
		return Math.PI*Math.pow(this.radio, 2);
	}

	
	public double calcularPerimetro() {
		return 2*Math.PI*this.radio;
	}
	

}
