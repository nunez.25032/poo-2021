package pe.uni.edu.uni.fiis.semana5.figura;

public class CirculoNuevo extends Circulo {
	
	public double calcularArea() {
		
		return (Math.PI*Math.pow(this.getRadio(),2)+1);
	}
}
