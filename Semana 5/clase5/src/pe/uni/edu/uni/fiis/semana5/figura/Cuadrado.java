package pe.uni.edu.uni.fiis.semana5.figura;

public class Cuadrado /*implements Figurable*/extends Figura {
	private double lado;
	
	
	public double getLado() {
		return lado;
	}


	public void setLado(double lado) {
		this.lado = lado;
	}


	public double calcularArea() {
		
		return Math.pow(this.lado, 2);
	}

	
	public double calcularPerimetro() {
	
		return 4*this.lado;
	}
	
}
