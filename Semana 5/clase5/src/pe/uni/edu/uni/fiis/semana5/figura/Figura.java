package pe.uni.edu.uni.fiis.semana5.figura;

public abstract class Figura {
	private String nombre;
	public abstract double calcularArea();
	public abstract double calcularPerimetro();
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	//en las clases abstractas si puede tener atributos, en las interfaces no, solo se puede heredar un sola clase.
}
