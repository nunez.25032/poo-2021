package pe.uni.edu.uni.fiis.semana5.figura;

public interface Figurable {
	//Las interfaces no poseen atributos en cvambio las clases abstrafctas si tienen
	double calcularArea();
	double calcularPerimetro();
	
	

}
