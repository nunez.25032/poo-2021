package pe.uni.edu.uni.fiis.semana5.polimorfismo;

public class Aplicacion {
	public static void main(String[] args) {
		RepositorioBaseDatos repositorioBaseDatos = new RepositorioBaseDatos();
		Repositorio repositorio =  repositorioBaseDatos;
		Contable contable = repositorioBaseDatos;
		//similar al tipo de herencia, es como si repositorio y contable tiene una direccion a Repositorio de base de datos.
		//repostorio y contable solo pueden usar los metodos que heredan de su refrencia
		//repositorioBaseDatos tiene hereda de Repositorio Y Contable
		System.out.println(repositorio.obtenerRegistro("2"));
	}
}
