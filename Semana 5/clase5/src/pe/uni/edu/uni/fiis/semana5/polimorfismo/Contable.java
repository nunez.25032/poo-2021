package pe.uni.edu.uni.fiis.semana5.polimorfismo;

public interface Contable {
	public abstract int contar(int limite);
}
