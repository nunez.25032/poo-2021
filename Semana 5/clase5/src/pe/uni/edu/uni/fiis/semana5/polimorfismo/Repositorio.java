package pe.uni.edu.uni.fiis.semana5.polimorfismo;

public abstract class Repositorio {
	//usar la clase abstract me obliga que tengo que heredar esta clase
	private String conexion;
	
	public String obtenerRegistro(String valor) {
		return "uno";
	}

	public String getConexion() {
		return conexion;
	}

	public void setConexion(String conexion) {
		this.conexion = conexion;
	}
	
	

}
