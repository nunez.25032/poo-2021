package pe.uni.edu.uni.fiis.semana5.polimorfismo;

public class RepositorioBaseDatos extends Repositorio implements Contable{

	public String obtenerRegistro(String valor) {
		if(valor.equals("cero")) {
			return "0";
		}
		else {
			return "otro valor";
		}
			
		
	}

	public int contar(int limite) {
		int cuenta=0;
		for (int i = 0; i < limite; i++) {
			cuenta += i;
		}
		return cuenta;
	}
	
}
