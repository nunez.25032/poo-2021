package pe.uni.edu.uni.fiis.semana5.singleton;

public abstract class SingletonVehiculo {
	private static Vehiculo vehiculo;
	
	public static Vehiculo getInstancia() {
		if(vehiculo == null) {
			vehiculo = new Vehiculo("DUALIPA", 15.7, 9);
		}
		return vehiculo;
	}
	//el singleton te permite generar un objeto especifico de una clase que sea unica y se va acceder por medio
	//de un metodo llamadao "Instancia" que va ser estatico al igual que el obtejo que pertence a esta clase por lo tantro va ser
	//abstracto
}
