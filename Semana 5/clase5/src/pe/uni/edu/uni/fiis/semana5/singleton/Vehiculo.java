package pe.uni.edu.uni.fiis.semana5.singleton;
//vehiculo unico(objeto unico)
public class Vehiculo {
	private String placa;
	private double velocidad;
	private double posicion;
	
	Vehiculo(String placa, double velocidad, double posicion) {
		super();
		this.placa = placa;
		this.velocidad = velocidad;
		this.posicion = posicion;
	}
	
	
	public void avanzar(int pasos) {
		this.posicion = this.posicion + (this.velocidad*pasos);
	}
	
	
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public double getVelocidad() {
		return velocidad;
	}
	public void setVelocidad(double velocidad) {
		this.velocidad = velocidad;
	}
	public double getPosicion() {
		return posicion;
	}
	public void setPosicion(double posicion) {
		this.posicion = posicion;
	}
	
	
	
}
