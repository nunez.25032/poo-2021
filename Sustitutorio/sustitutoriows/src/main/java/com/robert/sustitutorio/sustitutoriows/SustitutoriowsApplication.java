package com.robert.sustitutorio.sustitutoriows;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SustitutoriowsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SustitutoriowsApplication.class, args);
    }

}
