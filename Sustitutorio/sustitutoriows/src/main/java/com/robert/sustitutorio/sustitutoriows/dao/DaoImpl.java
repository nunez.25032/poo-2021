package com.robert.sustitutorio.sustitutoriows.dao;



import com.robert.sustitutorio.sustitutoriows.dto.modell.Curso;
import com.robert.sustitutorio.sustitutoriows.dto.modell.Estudiante;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void cerrarConexion(ResultSet resultado, Statement sentencia){
        try {
            if(resultado != null){
                resultado.close();
            }
            if(sentencia != null){
                sentencia.close();
            }
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    public Curso auteticarEstudiante(Estudiante estudiante, Curso curso) {
        curso = new Curso();
        String SQL= " SELECT nombre,precio,fecha_inicio,fecha_fin\n" +
                " FROM curso c\n" +
                " JOIN inscripcion_curso i ON (c.id_curso = i.id_curso)\n" +
                " JOIN estudiante e ON (e.id_estudiante = i.id_estudiante)\n" +
                " WHERE correo = ? AND clave = ?";
        try {
            obtenerConexion();
            PreparedStatement sentencia = this.conexion.prepareStatement(SQL);
            sentencia.setString(1,estudiante.getCorreo());
            sentencia.setString(2,estudiante.getClave());
            ResultSet resultado = sentencia.executeQuery();
            while (resultado.next()){
                curso = extraerCurso(resultado);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return curso;
    }


    private Curso extraerCurso(ResultSet resultado) throws SQLException {
        Curso retorno = new Curso(
                resultado.getInt("idCurso"),
                resultado.getString("nombre"),
                resultado.getDouble("precio"),
                resultado.getString("fechaInicio"),
                resultado.getString("fechaFin")
        );
        return retorno;
    }


}
