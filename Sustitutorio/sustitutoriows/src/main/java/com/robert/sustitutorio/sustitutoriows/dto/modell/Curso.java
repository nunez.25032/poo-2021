package com.robert.sustitutorio.sustitutoriows.dto.modell;

public class Curso {
    private Integer idCurso;
    private String nombre;
    private Double precio;
    private String fechaInicio;
    private String fechaFin;

    public Curso(Integer idCurso, String nombre, Double precio, String fechaInicio, String fechaFin) {
        this.idCurso = idCurso;
        this.nombre = nombre;
        this.precio = precio;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    public Curso(){

    }
    /*
    id_curso NUMERIC(9) PRIMARY KEY,
	nombre VARCHAR(200),
	precio NUMERIC(9,2),
	fecha_inicio VARCHAR(10),
	fecha_fin VARCHAR(10)
     */
}
