package com.robert.sustitutorio.sustitutoriows.dto.modell;

public class Estudiante {
    private Integer idEstudiante;
    private String nombres;
    private String apellidos;
    private String correo;
    private String clave;

    public Estudiante(Integer idEstudiante, String nombres, String apellidos, String correo, String clave) {
        this.idEstudiante = idEstudiante;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.clave = clave;
    }

    /*
    id_estudiante NUMERIC(9) PRIMARY KEY,
	nombres VARCHAR(200),
	apellidos VARCHAR(200),
	correo VARCHAR(200) UNIQUE,
	clave VARCHAR(200)
     */
}
