package com.robert.sustitutorio.sustitutoriows.servicio;

import com.robert.sustitutorio.sustitutoriows.dto.modell.Curso;
import com.robert.sustitutorio.sustitutoriows.dto.modell.Estudiante;

import java.util.List;

public interface Servicio {
    public Curso auteticarEstudiante(Estudiante estudiante, Curso curso);

}
