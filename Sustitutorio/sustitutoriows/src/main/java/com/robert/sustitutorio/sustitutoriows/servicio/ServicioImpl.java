package com.robert.sustitutorio.sustitutoriows.servicio;

import com.robert.sustitutorio.sustitutoriows.dao.Dao;
import com.robert.sustitutorio.sustitutoriows.dto.modell.Curso;
import com.robert.sustitutorio.sustitutoriows.dto.modell.Estudiante;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ServicioImpl  implements Servicio{
    @Autowired
    private Dao dao;

    public Curso auteticarEstudiante(Estudiante estudiante, Curso curso) {
        return dao.auteticarEstudiante(estudiante,curso);
    }

}
