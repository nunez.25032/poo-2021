package resolsucion;


public class elementos {
	private double numero;
	private int base;
	private double numeroABase10;
	private double numeroABaseX;
	private int baseConverir;
	private boolean bien;

	

	public int getBaseConverir() {
		return baseConverir;
	}

	public void setBaseConverir(int baseConverir) {
		this.baseConverir = baseConverir;
	}

	public double getNumero() {
		return numero;
	}

	public double getNumeroABase10() {
		return numeroABase10;
	}

	public void setNumeroABase10(double numeroABase10) {
		this.numeroABase10 = numeroABase10;
	}

	public double getNumeroABaseX() {
		return numeroABaseX;
	}

	public void setNumeroABaseX(double numeroABaseX) {
		this.numeroABaseX = numeroABaseX;
	}

	public void setNumero(double numero) {
		this.numero = numero;
	}

	public int getBase() {
		return base;
	}

	public void setBase(int base) {
		this.base = base;
	}
	public boolean isBien() {
		return bien;
	}

	public void setBien(boolean bien) {
		this.bien = bien;
	}
	
	
	
	public Double convertidoraABase10() {
		double numeroConvertidoaBase10;
		double respaldo=numero;
		double resto;
		int punto=0;
		while(respaldo>0) {
			resto=respaldo%10;
			respaldo=respaldo/10;
			numeroABase10=numeroABase10+resto*Math.pow(base, punto);
			punto++;
			
		}
		numeroConvertidoaBase10=numeroABase10;
		return numeroConvertidoaBase10;
	}
	
	public Double convertidorABase9() {
		double numeroConvertidoABaseX;
		int seguir=1;
		double contenedor=numeroABase10;
		while(seguir==1) {
			int i=1;
			int cont=0;
			double corredor=0;
			while(contenedor>i) {
				i=i*baseConverir;
				cont++;
			}
			corredor=Math.pow(10, cont);
			numeroABaseX=numeroABaseX+corredor;
			contenedor=contenedor-i;
			if(contenedor==0) {
				seguir=0;
			}
		}
		
		numeroConvertidoABaseX=numeroABaseX;
		
		return numeroConvertidoABaseX;
	}
	

}




