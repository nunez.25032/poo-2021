package pe.edu.uni.fiis.lab4.problema1;

public class Animal {
	protected String volumen;
	protected String masa;
	protected String altura;
	protected String nombre;
	public Animal(String volumen, String masa, String altura, String nombre) {
		super();
		this.volumen = volumen;
		this.masa = masa;
		this.altura = altura;
		this.nombre = nombre;
		if(this.getClass() == Animal.class) {
			System.out.println(this.nombre + " Pertenece a los mamiferos");
		}
		
	}
	
	
	
}
