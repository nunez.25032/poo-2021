package pe.edu.uni.fiis.lab4.problema1;

public class Mamifero extends Animal {
	private String habitad;
	private String Comida;
	public Mamifero(String volumen, String masa, String altura, String nombre, String habitad, String comida) {
		super(volumen, masa, altura, nombre);
		this.habitad = habitad;
		this.Comida = comida;
		
		System.out.println(this.nombre + " pertenece a mamifero" );
	}
	
	
}
