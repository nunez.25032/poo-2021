package pe.edu.uni.fiis.lab4.problema2;

public class Numero {
	
	public static void operacion(int x, int y) {
		int a = x + y;
		System.out.println("la suma es: "+ a);
	}
	
	public static void operacion(double x, double y) {
		double a = x / y;
		System.out.println("la division es: "+ a);
	}
	
	public static void operacion(long x, long y) {
		long a = x - y;
		System.out.println("la resta es: "+ a);
	}
	
	public static void operacion(float x, float y) {
		float a = x * y;
		System.out.println("la multiplicacion es: "+ a);
	}
	
}
