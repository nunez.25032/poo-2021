package pe.edu.uni.fiis.lab4.problema3;

public class Aplicacion {
	public static void main(String[] args) {
		
		Fibonacci.pintarNumeros(5);
		Fibonacci.sumaNumeros(5);
		System.out.println("-------------------------------");
		FibonacciIntercambiado.numeroIntercambiado(5);
		FibonacciIntercambiado.sumaIntercambiada(5);
		
	}

}
