package pe.edu.uni.fiis.lab4.problema3;

public class Fibonacci {
	public static void pintarNumeros(int termino) {
		int num1=0; 
		int num2=1;
		int aux;
		
		for(int i = 0; i < termino; i++) {
			if(i == 0) {
				System.out.println(num1);	
			}
			else if(i == 1) {
				System.out.println(num2 );
			}
			else {
				aux=num1+num2;
				num1=num2;
				num2=aux;
				System.out.println(aux);
			}
		}
	}
	public static void sumaNumeros(int termino) {
		int num1=0; 
		int num2=1;
		int aux;
		long total=0;
		
		for(int i = 0; i < termino; i++) {
			if(i == 0) {
				
				total = total + num1;
			}
			else if(i == 1) {
				
				total = total + num2;
			}
			else {
				aux=num1+num2;
				num1=num2;
				num2=aux;
				total=total+aux;
			}
		}
		System.out.println("La suma es "+ total);
	}
	
	

}
