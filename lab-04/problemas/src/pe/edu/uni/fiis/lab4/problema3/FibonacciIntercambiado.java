package pe.edu.uni.fiis.lab4.problema3;

public class FibonacciIntercambiado {
	public static void numeroIntercambiado(int termino) {
		int num1=0;
		int num2=1;
		int aux=0;
		for(int i = 0; i < termino; i++) {
			if(i == 0) {
				System.out.print(num1);
			}
			else if(i == 1) {
				System.out.print((-1)*num2);
			}
			else if(i % 2 == 0) {
				aux=num1+num2;
				num1=num2;
				num2=aux;
				System.out.print(aux );
			}
			else if(i % 2 != 0) {
				aux=num1+num2;
				num1=num2;
				num2=aux;
				System.out.print((-1)*aux);
			}
		}
	}
	
	public static void sumaIntercambiada(int termino) {
		int num1=0;
		int num2=1;
		int aux=0;
		long total=0;
		for(int i = 0; i < termino; i++) {
			if(i == 0) {
				total = total + num1;
			}
			else if(i == 1) {
				total = total +num2*(-1);
			}
			else if(i % 2 == 0) {
				aux=num1+num2;
				num1=num2;
				num2=aux;
				total = total +aux;
			}
			else if(i % 2 != 0) {
				aux=num1+num2;
				num1=num2;
				num2=aux;
				total = total +aux*(-1);
			}
		}
		System.out.println(total);
	}
	
}
