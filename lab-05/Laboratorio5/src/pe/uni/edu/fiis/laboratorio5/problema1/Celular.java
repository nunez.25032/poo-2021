package pe.uni.edu.fiis.laboratorio5.problema1;

public class Celular {
	private String color;
	private String marca;
	private double entrada;
	public Celular(String color, String marca, double entrada) {
		super();
		this.color = color;
		this.marca = marca;
		this.entrada = entrada;
	}
	
	public void mostrarCelular() {
		System.out.println("Marca: "+this.marca+"\t"+"Color: "+"\t");
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public double getEntrada() {
		return entrada;
	}
	public void setEntrada(double entrada) {
		this.entrada = entrada;
	}
	
	
	 
	
	
	
	
	

}
