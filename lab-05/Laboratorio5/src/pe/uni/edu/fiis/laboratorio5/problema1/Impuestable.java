package pe.uni.edu.fiis.laboratorio5.problema1;

public interface Impuestable {
	public abstract double calcularImpuesto();
}
