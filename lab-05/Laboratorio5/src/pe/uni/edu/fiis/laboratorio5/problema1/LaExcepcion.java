package pe.uni.edu.fiis.laboratorio5.problema1;

public class LaExcepcion extends Exception {
	public LaExcepcion(String message) {
		super(message);
	}
}
