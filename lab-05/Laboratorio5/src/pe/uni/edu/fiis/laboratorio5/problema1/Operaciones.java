package pe.uni.edu.fiis.laboratorio5.problema1;

public class Operaciones implements Impuestable {
	private Ventas venta;
	private double precioReal;
	private double precioImpuesto;
	
	public double calcularImpuesto() {
		int i=0;
		for (Celular celular : this.venta.getCelular()) {
			precioReal = celular.getEntrada()*this.venta.getPrecio()[i];
			precioImpuesto= precioImpuesto+precioReal*0.18;
		}
		return precioImpuesto;
	}

	public Ventas getVenta() {
		return venta;
	}

	public void setVenta(Ventas venta) {
		this.venta = venta;
	}

	public double getPrecioReal() {
		return precioReal;
	}

	public void setPrecioReal(double precioReal) {
		this.precioReal = precioReal;
	}

	public double getPrecioImpuesto() {
		return precioImpuesto;
	}

	public void setPrecioImpuesto(double precioImpuesto) {
		this.precioImpuesto = precioImpuesto;
	}

	
	
	
	
	

}
