package pe.uni.edu.fiis.laboratorio5.problema3;

public interface Calculable {
	public abstract double precioImpuesto(Celular celu);
}
