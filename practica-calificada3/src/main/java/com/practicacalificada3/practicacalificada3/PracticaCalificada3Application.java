package com.practicacalificada3.practicacalificada3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticaCalificada3Application {

    public static void main(String[] args) {
        SpringApplication.run(PracticaCalificada3Application.class, args);
    }

}
