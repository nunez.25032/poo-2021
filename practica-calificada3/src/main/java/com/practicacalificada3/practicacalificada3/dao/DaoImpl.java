package com.practicacalificada3.practicacalificada3.dao;

import com.practicacalificada3.practicacalificada3.dto.Actividad;
import com.practicacalificada3.practicacalificada3.dto.Departamento;
import com.practicacalificada3.practicacalificada3.dto.Empleado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;

    private void crearConexion(){
        try {
            conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void cerrarConexion(){
        try {
            conexion.commit();
            conexion.close();
            conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Empleado> obtenerEmpleados(Departamento departamento) {
        StringBuilder sb = new StringBuilder();
        sb.append(" select p.nombres, p.apellidos, d.nombre nombre_departamento").
        append(" from empleado p").
                append(" join departamento d on (p.codigo_departamento = d.codigo_departamento)");
        crearConexion();

        return null;
    }


    public Actividad registrarActividad(Actividad actividad) {
        return null;
    }


    public Empleado obtenerAsignacion() {
        return null;
    }


    public Departamento actualizarUbicacion(Departamento departamento) {
        return null;
    }
}
