package com.practicacalificada3.practicacalificada3.dto;

import lombok.Data;

@Data
public class Actividad {
    private String id_actividad;
    private String nombre;
    private Integer prioridad;
}
