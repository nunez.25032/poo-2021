package com.practicacalificada3.practicacalificada3.dto;

import lombok.Data;

@Data
public class Asignacion {
    private Integer id_asigancion;
    private String codigo_empleado;
    private Integer id_actividad;
    private Integer presupuesto;
}
