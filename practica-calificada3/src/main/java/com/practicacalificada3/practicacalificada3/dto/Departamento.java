package com.practicacalificada3.practicacalificada3.dto;

import lombok.Data;

@Data
public class Departamento {
    private String codigo_departamento;
    private String nombre;
    private String ubicacion;
}


