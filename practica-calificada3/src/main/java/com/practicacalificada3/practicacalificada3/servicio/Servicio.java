package com.practicacalificada3.practicacalificada3.servicio;

import com.practicacalificada3.practicacalificada3.dto.Actividad;
import com.practicacalificada3.practicacalificada3.dto.Departamento;
import com.practicacalificada3.practicacalificada3.dto.Empleado;

import java.util.List;

public interface Servicio {
    public List<Empleado> obtenerEmpleados(Departamento departamento);
    public Actividad registrarActividad(Actividad actividad);
    public Empleado obtenerAsignacion();
    public Departamento actualizarUbicacion(Departamento departamento);
}
