create table departamento(
                             codigo_departamento varchar(10) primary key,
                             nombre varchar(100),
                             ubicacion varchar(50)
);



create table empleado(
                         codigo_empleado varchar(20) primary key ,
                         codigo_departamento varchar(10),
                         nombres varchar(50),
                         apellidos varchar(100)
);


create table asignacion(
                           id_asignacion numeric(5) primary key ,
                           codigo_empleado varchar(20),
                           id_actividad numeric(4),
                           presupuesto numeric(9,2)
);


create table actividad(
                          id_actividad numeric(4) primary key ,
                          nombre varchar(50),
                          prioridad numeric(1)
);

alter table empleado add foreign key (codigo_departamento)
    references departamento(codigo_departamento);

alter table asignacion add foreign key (codigo_empleado)
    references empleado(codigo_empleado);

insert into departamento(codigo_departamento, nombre, ubicacion)
VALUES ('AXl8','Las casas','Miraflores');
insert into departamento(codigo_departamento, nombre, ubicacion)
VALUES ('RPT7','Los molinos','San Martin');
insert into departamento(codigo_departamento, nombre, ubicacion)
VALUES ('WRL2','Los santos','Independencia');

select *
from departamento;

-- Ahora con empleado
insert into empleado(codigo_empleado, codigo_departamento, nombres, apellidos)
VALUES ('20202071F','AXl8','Pedro','Cardozo');
insert into empleado(codigo_empleado, codigo_departamento, nombres, apellidos)
VALUES ('20202056T','AXl8','Lucho','Fernandez');
insert into empleado(codigo_empleado, codigo_departamento, nombres, apellidos)
VALUES ('20202080P','RPT7','Jair','Gracia');
insert into empleado(codigo_empleado, codigo_departamento, nombres, apellidos)
VALUES ('20202010E','WRL2','Aron','Breton');

select *
from empleado;
-- Ahora la asignacion

insert into asignacion(id_asignacion, codigo_empleado, id_actividad, presupuesto)
VALUES (1111,'20202071F',222,185.20);
insert into asignacion(id_asignacion, codigo_empleado, id_actividad, presupuesto)
VALUES (2222,'20202056T',333,134.65);
insert into asignacion(id_asignacion, codigo_empleado, id_actividad, presupuesto)
VALUES (3333,'20202080P',444,100.41);
insert into asignacion(id_asignacion, codigo_empleado, id_actividad, presupuesto)
VALUES (4444,'20202010E',555,164.30);

select *
from asignacion;

-- Ahora la actividad

insert into actividad(id_actividad, nombre, prioridad)
VALUES (222,'barrer',1);
insert into actividad(id_actividad, nombre, prioridad)
VALUES (333,'cocinar',2);
insert into actividad(id_actividad, nombre, prioridad)
VALUES (444,'vigilar',3);
insert into actividad(id_actividad, nombre, prioridad)
VALUES (555,'limpieza total',4);
insert into actividad(id_actividad, nombre, prioridad)
VALUES (666,'mantener las plantas',2);

SELECT *
FROM actividad;

-- Obtener los nombres, apellidos y el nombre del departamento de todos los empleados.

select p.nombres, p.apellidos, d.nombre nombre_departamento
from empleado p
join departamento d on (p.codigo_departamento = d.codigo_departamento) ;

-- Registrar una nueva actividad.

insert into actividad(id_actividad, nombre, prioridad)
VALUES (777,'acomodar',3);

-- Obtener las asignaciones de un determinado empleado.

select p.nombres,ac.nombre nombre_actividad
from empleado p
         join asignacion a on(a.codigo_empleado = p.codigo_empleado)
         join actividad ac on(ac.id_actividad = a.id_actividad)
where p.codigo_empleado = '20202080P';

-- Actualizar la ubicación de un determinado departamento

update departamento
set ubicacion = 'Tahuantisuyo'
where codigo_departamento = 'WRL2';

select *
from departamento;