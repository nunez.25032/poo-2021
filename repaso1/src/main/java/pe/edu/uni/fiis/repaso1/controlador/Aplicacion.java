package pe.edu.uni.fiis.repaso1.controlador;

import pe.edu.uni.fiis.repaso1.dto.Persona;
import pe.edu.uni.fiis.repaso1.servicio.Respiracion;
import pe.edu.uni.fiis.repaso1.servicio.RespiracionTipoA;
import pe.edu.uni.fiis.repaso1.servicio.RespiracionTipoAB;
public class Aplicacion {
    public static void main(String[] args) {
        Persona angelo = new Persona("Angelo", "A");
        Persona robert = new Persona("Robert", "AB");

        Respiracion respiracion = null;
        procesar(respiracion,angelo);
        procesar(respiracion,angelo);
        procesar(respiracion,angelo);
        procesar(respiracion,robert);
        procesar(respiracion,robert);
    }
    private static void procesar(Respiracion respiracion, Persona persona){
        if(persona.getTipoSangre().equals("A")){
            respiracion = new RespiracionTipoA();
        }
        else if(persona.getTipoSangre().equals("AB")){
            respiracion = new RespiracionTipoAB();
        }

        respiracion.respirar(persona);
    }
}
