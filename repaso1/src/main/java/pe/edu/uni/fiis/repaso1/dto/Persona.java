package pe.edu.uni.fiis.repaso1.dto;

public class Persona {
    private String nombre;
    private String tipoSangre;
    private Integer cantidadOxigeno;

    public Persona(String nombre, String tipoSangre) {
        this.nombre = nombre;
        this.tipoSangre = tipoSangre;
        this.cantidadOxigeno = 0;
    }

    public Integer getCantidadOxigeno() {
        return cantidadOxigeno;
    }

    public void setCantidadOxigeno(Integer cantidadOxigeno) {
        this.cantidadOxigeno = cantidadOxigeno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoSangre() {
        return tipoSangre;
    }

    public void setTipoSangre(String tipoSangre) {
        this.tipoSangre = tipoSangre;
    }
}
