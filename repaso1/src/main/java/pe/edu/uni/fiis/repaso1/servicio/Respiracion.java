package pe.edu.uni.fiis.repaso1.servicio;

import pe.edu.uni.fiis.repaso1.dto.Persona;

public abstract class Respiracion {
    public abstract void respirar(Persona persona);
}
