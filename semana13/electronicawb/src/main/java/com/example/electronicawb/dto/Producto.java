package com.example.electronicawb.dto;

import lombok.Data;

@Data
public class Producto {
    private Integer id;
    private Marca marca;
    private String nombre;
    private Double peso;
    private Double precioRegular;
    private Double precioOferta;
    private Boolean oferta;
    private String descripcion;
    private Integer calificacion;
    private String url_imagen;

    public Producto(Integer id, Marca marca, String nombre, Double peso, Double precioRegular, Double precioOferta, Boolean oferta, String descripcion, Integer calificacion, String url_imagen) {
        this.id = id;
        this.marca = marca;
        this.nombre = nombre;
        this.peso = peso;
        this.precioRegular = precioRegular;
        this.precioOferta = precioOferta;
        this.oferta = oferta;
        this.descripcion = descripcion;
        this.calificacion = calificacion;
        this.url_imagen = url_imagen;
    }
}
