package com.example.electronicawb.dto;

import lombok.Data;

import java.util.List;

@Data
public class RespuestaProducto {
    private List<Producto> lista;
}
