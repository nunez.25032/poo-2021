package com.example.electronicawb.servicio;

import com.example.electronicawb.dto.Marca;
import com.example.electronicawb.dto.Producto;

import java.util.List;

public interface Servicio {
    public List<Producto> obtenerProducto();
    public Producto obtenerProducto(Producto producto);
    public Marca agregarMarca(Marca marca);
    public Producto agregarProducto(Producto producto);
    public Producto modificarProducto(Producto producto);
}
