package com.example.electronicawb.servicio;

import com.example.electronicawb.dao.Dao;
import com.example.electronicawb.dto.Marca;
import com.example.electronicawb.dto.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao dao;
    public List<Producto> obtenerProducto() {
        return dao.obtenerProducto();
    }

    public Producto obtenerProducto(Producto producto) {
        return dao.obtenerProducto(producto);
    }


    public Marca agregarMarca(Marca marca) {
        return null;
    }


    public Producto agregarProducto(Producto producto) {
        return null;
    }


    public Producto modificarProducto(Producto producto) {
        return null;
    }
}
