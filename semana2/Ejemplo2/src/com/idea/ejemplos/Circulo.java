package com.idea.ejemplos;

import java.util.Scanner;

public class Circulo {
	private Double radio;

	public Double getRadio() {
		return radio;
	}

	public void setRadio(Double radio) {
		this.radio = radio;
	}
	
	public Double calcularArea() {
		return Math.PI*Math.pow(radio, 2);
	}
	
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		Circulo objetoCirculo = new Circulo();
		System.out.println("Ingrese el radio: ");
		objetoCirculo.setRadio(entrada.nextDouble());
		System.out.println("El area es: " + objetoCirculo.calcularArea());	
		System.out.println("El area pero en circulo 2: "+ Circulo2.calcularAreaDelCirculo(objetoCirculo));
	}	
}

