package com.idea.ejemplos;

public class Circulo2 {
	/*public static Double calcularAreaDelCirculo(Double radio) {
		return Math.PI*Math.pow(radio, 2);
	}*/
	public static Double calcularAreaDelCirculo(Circulo objetoCirculo) {
		return Math.PI*Math.pow(objetoCirculo.getRadio(), 2);
	}
}
