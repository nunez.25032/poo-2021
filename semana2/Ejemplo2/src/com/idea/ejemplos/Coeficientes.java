package com.idea.ejemplos;

import java.util.Scanner;

public class Coeficientes {
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		double coeficiente1,coeficiente2,coeficiente3;
		
		System.out.println("Primer coeficiente: ");
		coeficiente1 = entrada.nextDouble();
		
		System.out.println("Segundo coeficiente: ");
		coeficiente2 = entrada.nextDouble();
		
		System.out.println("Tercer coeficiente: ");
		coeficiente3 = entrada.nextDouble();
		
		CalcularRaices(coeficiente1, coeficiente2, coeficiente3);
	}
	
	
	public static void CalcularRaices(double coef1,double coef2,double coef3){
		double x1=((-coef2+Math.pow((Math.pow(coef2, 2)-4*coef1*coef3),0.5)))/(2*coef1);
		double x2=((-coef2-Math.pow((Math.pow(coef2, 2)-4*coef1*coef3),0.5)))/(2*coef1);
		if (Double.isNaN(x1)) {
			System.out.println("La primera raiz no existe ");
			if(Double.isNaN(x2)) {
				System.out.println("La segunda raiz no existe ");
			}
			else {
				System.out.println("La segunda raiz es"+x2);
			}
		}
		else {
			System.out.println("La primera raiz es: "+x1);
			if(Double.isNaN(x2)) {
				System.out.println("La segunda raiz no existe");
			}
			else {
				System.out.println("La segunda raiz es: "+x2);
			}
		}
	}
	
}
