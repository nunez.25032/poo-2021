package problemaCesar;


public class cadenaCesar {
	private String texto;

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	public String cifrarElCesar() {
		String oracionCifrada="";
		
		char [] abecedarioM= {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
		
		
		for (int i = 0; i < texto.length(); i++) {
			int punto=1;
			for (int j = 0; j < abecedarioM.length; j++) {
				if(texto.charAt(i)==abecedarioM[j]) {
				
					if(j+3>26) {
						oracionCifrada=oracionCifrada+abecedarioM[j+3-26];
					}
					else {
						oracionCifrada=oracionCifrada+abecedarioM[j+3];
					}
				}
				else if(texto.charAt(i)==' ' && punto==1) {
					oracionCifrada=oracionCifrada+" ";
					punto=0;
				}
			}
			
		}
		
		return oracionCifrada;
		
	}
		
		
}
