package semana2.laboratorio.problema3.aplicacion;

import java.util.Scanner;

import semana2.laboratorio.problema3.figura1.Triangulo;
import semana2.laboratorio.problema3.figura2.Cuadrado;
import semana2.laboratorio.problema3.figura3.Rectangulo;
import semana2.laboratorio.problema3.figura4.Rombo;

public class Aplicacion {
	public static void main(String[] args) {
		Triangulo objetoTriangulo=new Triangulo();
		objetoTriangulo.setAltura(4d);
		objetoTriangulo.setBase(5d);
		objetoTriangulo.setLado(6d);
		System.out.println("Area del triangulo: "+ objetoTriangulo.calcularAreaTriangulo());
		System.out.println("Perimetro del triangulo: "+ objetoTriangulo.calcularPerimetroTriangulo());
		
		Cuadrado objetoCuadrado = new Cuadrado();
		objetoCuadrado.setLado(8d);
		System.out.println("Area del cuadrado: "+ objetoCuadrado.calcularAreaCuadrado());
		System.out.println("Perimetro del cuadrado: "+ objetoCuadrado.calcularPerimetroCuadrado());
		
		Rectangulo objetoRectangulo = new Rectangulo();
		objetoRectangulo.setAncho(7d);
		objetoRectangulo.setLargo(9d);
		System.out.println("Area del rectangulo: "+ objetoRectangulo.calcularAreaRectangulo());
		System.out.println("Perimetro del rectangulo: "+ objetoRectangulo.calcularPerimetroRectangulo());
		
		Rombo objetoRombo=new Rombo();
		objetoRombo.setAltura(6d);
		objetoRombo.setLado(12d);
		System.out.println("Area del Rombo: "+ objetoRombo.calcularAreaRombo());
		System.out.println("Perimetro del Rombo: "+ objetoRombo.calcularPerimetroRombo());
	}
}
