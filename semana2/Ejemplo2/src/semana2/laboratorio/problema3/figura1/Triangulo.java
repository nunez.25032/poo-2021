package semana2.laboratorio.problema3.figura1;

public class Triangulo {
	private double base;
	private double altura;
	private double lado;
	
	public double getBase() {
		return base;
	}
	public void setBase(double base) {
		this.base = base;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public double getLado() {
		return lado;
	}
	public void setLado(double lado) {
		this.lado = lado;
	}
	
	public double calcularAreaTriangulo() {
		return (altura*base)/2;
	}
	
	public double calcularPerimetroTriangulo() {
		return lado*3;
	}
	
}
