package semana2.laboratorio.problema3.figura2;

public class Cuadrado {
	private double lado;

	public double getLado() {
		return lado;
	}

	public void setLado(double lado) {
		this.lado = lado;
	}
	
	public double calcularAreaCuadrado() {
		return lado*lado;
	}
	
	public double calcularPerimetroCuadrado() {
		return 4*lado;
	}
}
