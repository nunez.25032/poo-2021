package semana2.laboratorio.problema3.figura3;

public class Rectangulo {
	private double ancho;
	private double largo;
	public double getAncho() {
		return ancho;
	}
	public void setAncho(double ancho) {
		this.ancho = ancho;
	}
	public double getLargo() {
		return largo;
	}
	public void setLargo(double largo) {
		this.largo = largo;
	}
	
	public double calcularAreaRectangulo() {
		return ancho*largo;
	}
	
	public double calcularPerimetroRectangulo() {
		return 2*(ancho+largo);
	}
	
}
