package semana2.laboratorio.problema3.figura4;

public class Rombo {
	static Integer marcar;
	private double lado;
	private double altura;
	public double getLado() {
		return lado;
	}
	public void setLado(double lado) {
		this.lado = lado;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	
	public double calcularAreaRombo() {
		return lado*altura;
	}
	
	public double calcularPerimetroRombo() {
		return 2*(lado+altura);
	}
	
}
