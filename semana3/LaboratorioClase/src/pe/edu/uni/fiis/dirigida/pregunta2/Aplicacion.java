package pe.edu.uni.fiis.dirigida.pregunta2;


import java.util.Scanner;

public class Aplicacion {
	
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		
		Ciclo ciclo = new Ciclo();
		ciclo.setCursos(asignarCursos(entrada));
		ciclo.setPromedio(Util.obtenerPromedio(ciclo.getCursos()));
		System.out.println("Promedio del ciclo: "+ciclo.getPromedio());
	}
	
	public static Curso[] asignarCursos(Scanner entrada) {
		int cantidadCursos=0;
		System.out.println("Ingrese la cantidad de cursos: ");
		cantidadCursos=entrada.nextInt();
		Curso[] cursos = new Curso[cantidadCursos];
		
		for(int i = 0; i<cursos.length; i++) {
			System.out.println("Datos del curso: ");
			System.out.println("nombre: ");
			String nombre = entrada.nextLine();
			System.out.println("Creditos: ");
			int creditos = entrada.nextInt();
			cursos[i] = new Curso();
			cursos[i].setNombreCurso(nombre);
			cursos[i].setCreditos(creditos);
			cursos[i].setSistemaEvaluacion("F");
			cursos[i].setNotas(asignarNotas(entrada));;
		}
		return cursos;
	}
	
	public static Nota[] asignarNotas(Scanner entrada) {
		System.out.println("Ingrese cantidad de notas: ");
		int cantidadNotas = entrada.nextInt();
		Nota[] notas = new Nota[cantidadNotas];
		for(int j=0;j<notas.length;j++) {
			System.out.println("Datos de la Nota: ");
			System.out.println("Tipo: ");
			String tipo = entrada.next();
			System.out.println("Valor: ");
			int valor =  entrada.nextInt();
			notas[j] = new Nota();
			notas[j].setTipo(tipo);
			notas[j].setValor(valor);
		}
		return notas;
	}
}
