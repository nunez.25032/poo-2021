package pe.edu.uni.fiis.dirigida.pregunta2;

public class Curso {
	private String nombreCurso;
	private int creditos;
	private Nota[] notas;
	//por restricción del problema
	public String sistemaEvaluacion;
	public String getNombreCurso() {
		return nombreCurso;
	}
	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}
	public int getCreditos() {
		return creditos;
	}
	public void setCreditos(int creditos) {
		this.creditos = creditos;
	}
	public Nota[] getNotas() {
		return notas;
	}
	public void setNotas(Nota[] notas) {
		this.notas = notas;
	}
	public String getSistemaEvaluacion() {
		return sistemaEvaluacion;
	}
	public void setSistemaEvaluacion(String sistemaEvaluacion) {
		this.sistemaEvaluacion = sistemaEvaluacion;
	}
	
	
	
}
