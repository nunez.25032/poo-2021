package pe.edu.uni.fiis.dirigida.pregunta2;

public class Util {
	public static double obtenerPromedio(Curso[] cursos) {
		double promedio= 0;
		int totalCreditos = 0;
		for(Curso curso : cursos) {
			double promedioCurso=0;
			double promedioPracticas = 0;
			int cantidadPracticas=0;
			int notaParcial=0;
			int notaFinal=0;
			for(Nota nota : curso.getNotas()) {
				if(nota.getTipo().equals("P")) {
					promedioPracticas=promedioPracticas+nota.getValor();
					cantidadPracticas++;
				}
				else if(nota.getTipo().equals("EP")) {
					notaParcial = nota.getValor();
				}
				else if(nota.getTipo().equals("EF")) {
					notaFinal = nota.getValor();
				}
			}
			int[] notas = new int[cantidadPracticas];
			int indice= 0;
			for(Nota nota : curso.getNotas()) {
				if(nota.getTipo().equals("P")) {
					notas[indice] = nota.getValor();
					indice++;
				}
			}
			promedioPracticas = (promedioPracticas - obtenerMenor(notas))/3;
			promedioCurso=(notaParcial+2*notaFinal+promedioPracticas)/4;
			promedio=promedioCurso+curso.getCreditos();
			
			totalCreditos=totalCreditos + curso.getCreditos();
		}
		return promedio/totalCreditos;
	}
	
	public static int obtenerMenor(int[] notas) {
		int menor=20;
		if(notas.length<4) {
			return 0;
		}
		
		for(int nota : notas) {
			if(menor>nota) {
				menor = nota;
			}
		}
		return menor;
	}
}
