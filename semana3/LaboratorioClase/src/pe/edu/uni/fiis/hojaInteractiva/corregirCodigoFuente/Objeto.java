package pe.edu.uni.fiis.hojaInteractiva.corregirCodigoFuente;

class Objeto {
	private Objeto() {
		
	}
	Objeto(int x){
		
	}
	
}

 class Temp extends Objeto{
	Temp(){
		this(5);
		System.out.println("Constructor por defecto");
	}
	
	Temp(int x){
		this(5, 15);
		System.out.println(x);
	}
	
	Temp(int x,int y){
		super(x*y);
		System.out.println(x*y);
	}
	
	public String mensaje(String args[]) {
		Temp r = new Temp();
		return "ok";
	}
}