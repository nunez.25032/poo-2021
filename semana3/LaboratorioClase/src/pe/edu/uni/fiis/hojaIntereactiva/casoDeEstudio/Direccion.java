package pe.edu.uni.fiis.hojaIntereactiva.casoDeEstudio;

public class Direccion {
	private String lugar;
	private String transporte;
	
	public Direccion(String lugar, String transporte) {

		this.lugar = lugar;
		this.transporte = transporte;
	}
	
	public void dondeVa() {
		System.out.println("El producto se traslada de Urubamba hacia a "+ this.lugar + ", via "+ this.transporte);
	}
	
	
}
