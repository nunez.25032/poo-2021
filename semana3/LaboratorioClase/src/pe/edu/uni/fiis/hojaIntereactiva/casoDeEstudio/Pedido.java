package pe.edu.uni.fiis.hojaIntereactiva.casoDeEstudio;

public class Pedido {
	private Cliente cliente;
	private Direccion direccion;
	private Producto[] productos;
	private Double precioEnTotal;
	
	public Pedido(Cliente cliente, Direccion direccion, Producto[] productos) {
		super();
		this.cliente = cliente;
		this.direccion = direccion;
		this.productos = productos;
	}
	
	public void obtenerPrecioTotal() {
		Double total = 0d;
		
		for (Producto producto : productos) {
			total = total + producto.precioDelProducto();
		}
		
		this.precioEnTotal=total;
		
		System.out.println("El precio total del pedido es: "+ this.precioEnTotal);
	
	}
	
	
}
