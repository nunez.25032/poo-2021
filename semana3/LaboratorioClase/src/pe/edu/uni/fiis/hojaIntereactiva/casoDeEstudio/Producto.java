package pe.edu.uni.fiis.hojaIntereactiva.casoDeEstudio;

public abstract class Producto {
	private String nombre;
	private Double costo;
	private Double cantidad;
	

	public Producto(String nombre, Double costo, Double cantidad) {
		this.nombre=nombre;
		this.costo=costo;
		this.cantidad=cantidad;
	}


	public void cantidadDelProducto() {
		System.out.println("La cantidad es "+ this.cantidad+ "Kg"+ " de "+ this.nombre);
		
	}
	
	public Double precioDelProducto() {
		return this.cantidad*this.costo;
	}
	
}
