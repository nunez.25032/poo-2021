package pe.edu.uni.fiis.laboratorio3.problema1;

public class Aplicacion {
	
	public static void main(String[] args) {
		
		Estudiante[] listaEstudiantes = new Estudiante[3];
		
		Estudiante estudiante1 = new Estudiante("Jair", "Garcia");
		Estudiante estudiante2 = new Estudiante("Lucho", "Lazo");
		Estudiante estudiante3 = new Estudiante("Edu", "Garrido");
		
		listaEstudiantes[0]=estudiante1;
		listaEstudiantes[1]=estudiante2;
		listaEstudiantes[2]=estudiante3;
		
		
		double[] listaNotas = new double[3];
		
		listaNotas[0]=14.5;
		listaNotas[1]=16.5;
		listaNotas[2]=11.5;
		
		Curso matematica = new Curso(listaNotas,5,"SI105",8);
		matematica.mostrarCurso();
		Sesion sesion1 = new Sesion(listaEstudiantes,matematica);
		sesion1.mostrarLista();
	}
	
	

	
}
