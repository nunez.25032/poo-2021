package pe.edu.uni.fiis.laboratorio3.problema1;

public class Curso {
	private double[] notas;
	private int creditos;
	private String codigo;
	private int horas;
	
	public Curso(double[] notas, int creditos, String codigo, int horas) {
		super();
		this.notas = notas;
		this.creditos = creditos;
		this.codigo = codigo;
		this.horas = horas;
	}
	
	
	public void mostrarCurso() {
		System.out.println("Codigo: " + this.codigo + " Creditos: " + this.creditos + " Horas totales: " + this.horas );
	}
	
	
	public double[] getNotas() {
		return notas;
	}
	public void setNotas(double[] notas) {
		this.notas = notas;
	}
	public int getCreditos() {
		return creditos;
	}
	public void setCreditos(int creditos) {
		this.creditos = creditos;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public int getHoras() {
		return horas;
	}
	public void setHoras(int horas) {
		this.horas = horas;
	}
	
	
	
	
	
	
}
