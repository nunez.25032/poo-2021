package pe.edu.uni.fiis.laboratorio3.problema1;

public class Sesion {
	private Estudiante[] estudiantes;
	private Curso curso;
	public Sesion(Estudiante[] estudiantes, Curso curso) {
		super();
		this.estudiantes = estudiantes;
		this.curso = curso;
	}
	
	public void mostrarLista() {
		System.out.println("Nombre\tApellidos\tNota");
		for(int i = 0; i<estudiantes.length; i++) {
			System.out.println(estudiantes[i].getNombre()+"\t"+estudiantes[i].getApellidos()+"\t\t"+curso.getNotas()[i]);
			
		}
	}
	
	public Estudiante[] getEstudiantes() {
		return estudiantes;
	}
	public void setEstudiantes(Estudiante[] estudiantes) {
		this.estudiantes = estudiantes;
	}
	public Curso getCurso() {
		return curso;
	}
	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	
	
}
