package pe.edu.uni.fiis.laboratorio3.problema2;

public class Aplicacion {
	public static void main(String[] args) {
		MotorGenerico motor1 = new MotorGenerico("a", "b", "c");
		MotorFluvial motor2 = new MotorFluvial("a", "b", "c", "d", "e");
		System.out.println("Motor Generico");
		motor1.potencia();
		motor1.resistencia();
		System.out.println("Motor Fluvial");
		motor2.potencia();
		motor2.resistencia();
	}
}
