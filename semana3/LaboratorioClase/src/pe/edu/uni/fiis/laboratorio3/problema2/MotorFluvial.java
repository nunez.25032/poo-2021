package pe.edu.uni.fiis.laboratorio3.problema2;

public class MotorFluvial extends MotorGenerico {
	private String componente1;
	private String componente2;
	
	public MotorFluvial(String primerComponente, String segundoComponenete, String tercerComponente, String componente1, String componente2){
		super(primerComponente,segundoComponenete,tercerComponente);
		this.componente1=componente1;
		this.componente2=componente2;
	}
	
	public void potencia() {
		System.out.println("La potencia se da por la combinacion de "+ this.componente1+ " y " + super.getPrimerComponente());
	}
	
	public void resistencia() {
		System.out.println("La resistencia se da por la combinacion de "+ this.componente2 +", "+ super.getTercerComponente()+ " y " + super.getSegundoComponenete());
	}

	public String getComponente1() {
		return componente1;
	}

	public void setComponente1(String componente1) {
		this.componente1 = componente1;
	}

	public String getComponente2() {
		return componente2;
	}

	public void setComponente2(String componente2) {
		this.componente2 = componente2;
	}
	
	
	
}
