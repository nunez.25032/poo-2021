package pe.edu.uni.fiis.laboratorio3.problema2;

public class MotorGenerico {
	private String primerComponente;
	private String segundoComponente;
	private String tercerComponente;
	
	public MotorGenerico(String primerComponente, String segundoComponente, String tercerComponente) {
		super();
		this.primerComponente = primerComponente;
		this.segundoComponente = segundoComponente;
		this.tercerComponente = tercerComponente;
	}
	
	
	public void potencia() {
		System.out.println("La potencia se da por la combinacion de "+ this.primerComponente+" y " + this.tercerComponente);
	}
	public void resistencia() {
		System.out.println("La resistencia se da por la combinacion de "+ this.segundoComponente+ " y " + this.tercerComponente);
	}
	
	

	public String getPrimerComponente() {
		return primerComponente;
	}

	public void setPrimerComponente(String primerComponente) {
		this.primerComponente = primerComponente;
	}

	public String getSegundoComponenete() {
		return segundoComponente;
	}

	public void setSegundoComponenete(String segundoComponenete) {
		this.segundoComponente = segundoComponenete;
	}

	public String getTercerComponente() {
		return tercerComponente;
	}

	public void setTercerComponente(String tercerComponente) {
		this.tercerComponente = tercerComponente;
	}
	
	
	

}
