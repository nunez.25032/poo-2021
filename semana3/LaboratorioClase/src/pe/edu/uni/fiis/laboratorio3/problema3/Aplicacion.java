
package pe.edu.uni.fiis.laboratorio3.problema3;

public class Aplicacion {
	public static void main(String[] args) {
		double[] raices = new double[2];
		double[] coeficientes = {1,2,1};
		
		EcuacionSegundoGrado ecuacion = new EcuacionSegundoGrado(coeficientes, raices);
		//ecuacion.discriminante();
		ecuacion.calcularRaices();
		ecuacion.mostrarRaices();
	}
}
