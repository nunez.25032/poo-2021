package pe.edu.uni.fiis.laboratorio3.problema3;

public class EcuacionSegundoGrado extends EsqueletoEcuacion {
	
	public EcuacionSegundoGrado(double[] coeficientes, double[] raices) {
		super(coeficientes, raices);
	}
	
	public double discriminante() {
		return (this.getCoeficientes()[1]*this.getCoeficientes()[1])-4*this.getCoeficientes()[0]*this.getCoeficientes()[2];
	
	}

	public void calcularRaices() {
		double[] raices = new double[2];
		if(discriminante()>0) {
			raices[0]=(-this.getCoeficientes()[1]+Math.sqrt(discriminante()))/(2*this.getCoeficientes()[0]);
			raices[1]=(-this.getCoeficientes()[1]-Math.sqrt(discriminante()))/(2*this.getCoeficientes()[0]);
		}
		else if(discriminante()==0) {
			raices[0]=raices[1]=-(this.getCoeficientes()[1])/(2*this.getCoeficientes()[0]);
		}
		else {
			System.out.println("Las raices no existen en los reales");
		}
	}
	
}
