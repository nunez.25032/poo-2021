package pe.edu.uni.fiis.laboratorio3.problema3;

public abstract class EsqueletoEcuacion {
	
	private double[] coeficientes;
	private double[] raices;
		
	public abstract void calcularRaices();
	public abstract double discriminante();
	
	public void mostrarRaices() {
		System.out.println("1� Raiz: "+ this.raices[0]);
		System.out.println("2� Raiz: "+ this.raices[1]);
	}
	

	public EsqueletoEcuacion(double[] coeficientes, double[] raices) {
		super();
		this.coeficientes = coeficientes;
		this.raices = raices;
	}

	public double[] getCoeficientes() {
		return coeficientes;
	}

	public void setCoeficientes(double[] coeficientes) {
		this.coeficientes = coeficientes;
	}

	public double[] getRaices() {
		return raices;
	}

	public void setRaices(double[] raices) {
		this.raices = raices;
	}
	
}
